module Text.Tamper.Tags
( tag
, tagX
, tagS
)
where

import Text.Tamper.Internal
import qualified Text.Tamper.DOM as DOM

tag :: (Monad m, Ord t) => t -> TamperT t m a -> TamperT t m a
tag name inner =
    nodeWithContent (DOM.elementNode name DOM.AutoClosingElement) inner

tagX :: (Monad m, Ord t) => t -> TamperT t m a -> TamperT t m a
tagX name inner =
    nodeWithContent (DOM.elementNode name DOM.ElaborateElement) inner

tagS :: (Monad m, Ord t) => t -> TamperT t m ()
tagS name =
    simpleNode (DOM.elementNode name DOM.SingletonElement)
