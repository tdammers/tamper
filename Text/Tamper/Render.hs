{-#LANGUAGE OverloadedStrings #-}
module Text.Tamper.Render
where

import Text.Tamper
import Data.Monoid
import Data.String
import qualified Text.Tamper.DOM as DOM

-- | renderTamperT runs a TamperT action, applies the provided reducer to
-- distill the resulting DOM tree into a string-like value, and returns that
-- value into the underlying monad.
renderTamperT :: (Monoid t, IsString t, Monad m) => (DOM.NodeList t -> t) -> TamperT t m a -> m t
renderTamperT reducer a = do
    (_, nodes) <- runTamperT a
    return $ reducer nodes
